package com.example.movieapp.model


import com.google.gson.annotations.SerializedName

//this data class represents the response of api https://swapi.dev/api/films/
data class ApiResponse(
    @SerializedName("count")
    val count: Int,
    @SerializedName("next")
    val next: Any,
    @SerializedName("previous")
    val previous: Any,
    @SerializedName("results")
    val movies:List<Movie>
)