package com.example.movieapp.model

import androidx.room.*
import com.example.movieapp.utils.Converters
import com.google.gson.annotations.SerializedName
import java.io.Serializable

//this data class represents the results data from https://swapi.dev/api/films/
@Entity(tableName = "movie_details")
public data class Movie(

    @PrimaryKey
    @SerializedName("episode_id")
    val episodeId:Int?,

    val title: String?,

    @SerializedName("characters")
    @TypeConverters(Converters::class)
    val characters: List<String>,

    @SerializedName("created")
    val created: String?,

    @SerializedName("director")
    val director: String?,

    @SerializedName("producer")
    val producer: String?,

    @SerializedName("release_date")
    val releaseDate: String?
)