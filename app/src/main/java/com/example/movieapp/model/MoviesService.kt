package com.example.movieapp.model

import io.reactivex.Single
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory
import java.net.URI

//Retrofit API Service
class MoviesService {


    private val BASE_URL = "https://swapi.dev/api/"
    private val api: MoviesApi

    init {
        api = Retrofit.Builder()        //creates framework for Retrofit
            .baseUrl(BASE_URL)
            .addConverterFactory(GsonConverterFactory.create())     //to convert received Json to required objects
            .addCallAdapterFactory(RxJava2CallAdapterFactory.create())      //to use observables and observes of RxJava
            .build()
            .create(MoviesApi::class.java)
    }

    //returns all the movies from given api endpoint
    fun getMovies(): Single<ApiResponse> {
        return api.getAllMovies()

    }

    //returns all the characters from given api endpoint and personId
    fun getCharacters(personUrl: String): Single<Character> {

        val personId = splitIdFromUrl(personUrl)
        return api.getCharacters(personId);

    }

    //function to extract person Id from person URL
    private fun splitIdFromUrl(personUrl: String): Int {
        val uri = URI(personUrl)
        val segments =
            uri.path.split("/".toRegex()).toTypedArray()
        val idStr = segments[segments.size - 2]

        return idStr.toInt()
    }


}

