package com.example.movieapp.model


import androidx.room.Entity
import androidx.room.OnConflictStrategy
import androidx.room.PrimaryKey
import com.google.gson.annotations.SerializedName

//this data class represents the character data from https://swapi.dev/api/persons/{id}
@Entity(tableName = "character_table")
data class Character(
    @SerializedName("birth_year")
    val birthYear: String,

    @SerializedName("gender")
    val gender: String,

    @SerializedName("height")
    val height: String,

    @SerializedName("mass")
    val mass: String,

    @SerializedName("name")
    val name: String,

    @PrimaryKey
    @SerializedName("url")
    val url: String
)