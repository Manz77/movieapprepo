package com.example.movieapp.model

import io.reactivex.Single
import retrofit2.http.GET
import retrofit2.http.Path

//Retrofit API interface
interface MoviesApi {

    //api endpoints for films
    @GET("films/")
    fun getAllMovies(): Single<ApiResponse>

    //api endpoint for person with inputted personId
    @GET("people/{personId}")
    fun getCharacters(@Path("personId") personId:Int): Single<Character>
}