package com.example.movieapp.db

import androidx.lifecycle.LiveData
import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import com.example.movieapp.model.Character

//data access object interface to communicate with character_table in database
@Dao
interface CharacterDAO {

    @Query("Select * from character_table where url = :url")
    fun getCharacter( url: String): Character?

    @Query("Select * from character_table")
    fun getAllCharacters(): LiveData<List<Character>>

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insert(character: Character)
}