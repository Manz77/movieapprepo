package com.example.movieapp.db

import androidx.lifecycle.LiveData
import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import com.example.movieapp.model.Movie


//data access object interface to communicate with movie_table in database
@Dao
interface MovieDAO {

    @Query("Select * from movie_details Order By CAST(releaseDate AS FLOAT) DESC")
    fun getAllMoviesFromDB(): LiveData<List<Movie>>

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insert(movie: Movie)

}