package com.example.movieapp.db

import android.content.Context
import androidx.room.Database
import androidx.room.Room
import androidx.room.RoomDatabase
import androidx.room.TypeConverters
import com.example.movieapp.model.Character
import com.example.movieapp.model.Movie
import com.example.movieapp.utils.Converters

//creating a database with two tables of dataclasses @Movie and @Character
@Database(entities = [Movie::class, Character::class],version = 1, exportSchema = false)
@TypeConverters(Converters::class)
public abstract class MyDatabase: RoomDatabase() {

    abstract fun movieDAO(): MovieDAO
    abstract fun characterDAO(): CharacterDAO

    companion object{
        // Singleton prevents multiple instances of database opening at the
        // same time.
        @Volatile
        private var INSTANCE: MyDatabase? = null

        fun getDatabase(context: Context): MyDatabase {
            val tempInstance = INSTANCE
            if (tempInstance != null) {
                return tempInstance
            }
            synchronized(this) {
                val instance = Room.databaseBuilder(
                    context.applicationContext,
                    MyDatabase::class.java,
                    "my_database"
                ).build()
                INSTANCE = instance
                return instance
            }
        }
    }
}