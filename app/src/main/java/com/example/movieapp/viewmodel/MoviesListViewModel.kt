package com.example.movieapp.viewmodel

import android.app.Application
import androidx.lifecycle.LiveData
import androidx.lifecycle.ViewModel
import com.example.movieapp.model.Movie
import com.example.movieapp.repository.MovieRepository
import com.example.movieapp.utils.NetworkState

class MoviesListViewModel(application: Application) : ViewModel() {

    private val movieRepository: MovieRepository = MovieRepository(application)

    //these live data will be observed by views
    val movies: LiveData<List<Movie>>
    val networkState: LiveData<NetworkState>

    init {
        movies = movieRepository.allMovies
        networkState = movieRepository.networkState
    }

    //dispose the rxjava thread using compositeDisposable after api call
    override fun onCleared() {
        super.onCleared()
        movieRepository.compositeDisposable.dispose()
    }



}