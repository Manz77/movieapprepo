package com.example.movieapp.viewmodel

import android.app.Application
import androidx.lifecycle.LiveData
import androidx.lifecycle.ViewModel
import com.example.movieapp.model.Movie
import com.example.movieapp.model.Character
import com.example.movieapp.repository.CharacterRepository
import com.example.movieapp.utils.NetworkState

class SingleMovieViewModel(application: Application, val movie: Movie): ViewModel() {

    private val characterRepository = CharacterRepository(application, movie)

    val allCharacter: LiveData<List<Character>>
    val networkState: LiveData<NetworkState>

    init {
        allCharacter = characterRepository.allCharacters
        networkState = characterRepository.networkState
    }

    //dispose the rxjava thread using compositeDisposable after api call
    override fun onCleared() {
        super.onCleared()
        characterRepository.compositeDisposable.dispose()
    }
}