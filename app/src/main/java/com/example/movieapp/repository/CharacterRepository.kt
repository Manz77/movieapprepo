package com.example.movieapp.repository

import android.app.Application
import android.os.AsyncTask
import android.util.Log
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.example.movieapp.db.CharacterDAO
import com.example.movieapp.model.Movie
import com.example.movieapp.db.MyDatabase
import com.example.movieapp.model.Character
import com.example.movieapp.model.MoviesService
import com.example.movieapp.utils.NetworkState
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.schedulers.Schedulers
import java.lang.Exception

//Repository for handling Character data
class CharacterRepository(application: Application, val movie: Movie) {

    var characterDAO: CharacterDAO
    private val moviesService = MoviesService()
    private val peopleUrls = movie.characters

    private val _allCharacterList =
        ArrayList<Character>()          //temp arraylist to add all the characters returned from api

    private var _allCharacters = MutableLiveData<List<Character>>()
    val allCharacters: LiveData<List<Character>>
        get() = _allCharacters                  //getter for LiveData<List<Character>>


    private val _networkState = MutableLiveData<NetworkState>()
    val networkState: LiveData<NetworkState>
        get() = _networkState                   //getter for LiveData<NetworkState>

    val compositeDisposable = CompositeDisposable();

    init {
        //get the database instance
        val db: MyDatabase = MyDatabase.getDatabase(application.applicationContext)!!
        characterDAO = db.characterDAO()            //DAO from the created DB

        //getAll the list of movies from DB matching the query and post it to Live data
        peopleUrls.forEachIndexed { index, element ->
            AsyncTask.execute {

                try {
                    val character = characterDAO.getCharacter(element)
                    if(character != null){
                        _allCharacterList.add(character)
                    }
                    if (index == peopleUrls.size - 1) {
                        _allCharacters.postValue(_allCharacterList);
                    }
                }
                catch (e:Exception){

                }

            }
        }

        //get all the people/character data from api
        peopleUrls.forEach {
            compositeDisposable.add(
                moviesService.getCharacters(it)
                    .subscribeOn(Schedulers.newThread())
                    .subscribe({ it ->
                        _networkState.postValue(NetworkState.LOADED)
                        insert(it)
                    }, {
                        _networkState.postValue(NetworkState.ERROR)
                        Log.e("Character Repo", it.message)
                    })
            )
        }

    }


    //insert the characters into the db
    private fun insert(character: Character) {
        characterDAO.insert(character)
    }
}