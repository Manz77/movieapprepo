package com.example.movieapp.repository

import android.app.Application
import android.util.Log
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.example.movieapp.model.Movie
import com.example.movieapp.db.MovieDAO
import com.example.movieapp.db.MyDatabase
import com.example.movieapp.model.MoviesService
import com.example.movieapp.utils.NetworkState
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.schedulers.Schedulers

//class to check whether to fetch movie data from local database or API
class MovieRepository(application: Application) {

    private var movieDAO: MovieDAO
    private val moviesService = MoviesService()

    val allMovies: LiveData<List<Movie>>               //allmovies LiveData

    private val _networkState = MutableLiveData<NetworkState>()
    val networkState: LiveData<NetworkState>
        get() = _networkState                       //getter for LiveData<Network>

    val compositeDisposable = CompositeDisposable();

    init {

        //get the database instance
        val db: MyDatabase = MyDatabase.getDatabase(application.applicationContext)!!
        movieDAO = db.movieDAO()            //DAO from the created DB

        //getAll the list of movies from DB
        allMovies = movieDAO.getAllMoviesFromDB()

        //get all the list of movie from api
        compositeDisposable.add(
            moviesService.getMovies()
                .subscribeOn(Schedulers.io())
                .subscribe({ it ->
                    _networkState.postValue(NetworkState.LOADED)
                    it.movies.forEach { film ->
                        insert(film)
                    }
                }, {
                    _networkState.postValue(NetworkState.ERROR)
                    Log.e("MovieRepo MovieDetails", it.message)
                })
        )

    }

    //insert the movies into the db
    private fun insert(movie: Movie) {
        movieDAO.insert(movie)
    }

}