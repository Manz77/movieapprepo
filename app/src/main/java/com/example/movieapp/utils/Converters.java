package com.example.movieapp.utils;

import androidx.room.TypeConverter;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import java.io.Serializable;
import java.util.List;

//this helper class is used to convert the data class object to json string and vice versa
public class Converters implements Serializable {
    @TypeConverter
    public static List<String> StringToListConverter(String listOfString) {
        return new Gson().fromJson(listOfString, new TypeToken<List<String>>() {}.getType());
    }

    @TypeConverter
    public static String ListToStringConverter(List<String> listOfString) {
        return new Gson().toJson(listOfString);
    }
}
