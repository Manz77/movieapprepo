package com.example.movieapp.utils


enum class Status{
    RUNNING,
    SUCCESS,
    FAILED
}

class NetworkState(val status: Status, val msg:String){

    //companion is used when we need a method static
    companion object{

        val LOADED: NetworkState = NetworkState(Status.SUCCESS, "Success")
        val LOADING: NetworkState = NetworkState(Status.RUNNING, "Loading")
        val ERROR: NetworkState = NetworkState(Status.FAILED, "Something went wrong")

    }
}