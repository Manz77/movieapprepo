package com.example.movieapp.view

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.example.movieapp.R
import com.example.movieapp.model.Character
import kotlinx.android.synthetic.main.item_characater.view.*

class CharacterListAdapter(var allCharacters: ArrayList<Character>) :
    RecyclerView.Adapter<CharacterListAdapter.CharacterViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int) = CharacterViewHolder(
        LayoutInflater.from(parent.context).inflate(R.layout.item_characater, parent, false)
    )

    override fun getItemCount(): Int {
        return allCharacters.size
    }

    override fun onBindViewHolder(holder: CharacterViewHolder, position: Int) {
            holder.bind(allCharacters[position])
    }

    fun updateCharacters(character: List<Character>) {
        allCharacters.clear()
        allCharacters.addAll(character)
        notifyDataSetChanged()
    }

    class CharacterViewHolder(view: View) : RecyclerView.ViewHolder(view) {

        private val characterName = view.character_name

        fun bind(character: Character) {
            characterName.text = character.name
        }
    }


}