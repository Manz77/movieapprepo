package com.example.movieapp.view

import android.annotation.SuppressLint
import android.content.Intent
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.example.movieapp.R
import com.example.movieapp.model.Movie
import com.google.gson.Gson
import kotlinx.android.synthetic.main.item_movie.view.*
import java.text.DateFormat
import java.text.ParseException
import java.text.SimpleDateFormat
import java.time.LocalDate
import java.time.format.DateTimeFormatter
import java.util.*

class MoviesListAdapter(private var movies: ArrayList<Movie>) :
    RecyclerView.Adapter<MoviesListAdapter.MovieViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int) = MovieViewHolder(
        LayoutInflater.from(parent.context).inflate(R.layout.item_movie, parent, false)
    )

    override fun getItemCount() = movies.size

    override fun onBindViewHolder(holder: MovieViewHolder, position: Int) {
        holder.bind(movies[position])
    }

    fun updateMovies(newMovies: List<Movie>) {
        movies.clear()
        movies.addAll(newMovies)
        notifyDataSetChanged()

    }

    class MovieViewHolder(view: View) : RecyclerView.ViewHolder(view) {

        private val movieName = view.name
        private val movieReleaseYear = view.release_year

        private val context = view.context
        private val iteView = view

        fun bind(movie: Movie) {
            movieName.text = movie.title
            movieReleaseYear.text = movie.releaseDate?.let { stringToDate(it) }

            iteView.setOnClickListener {
                val intent = Intent(context, SingleMovieActivity::class.java)
                intent.putExtra(
                    "movie",
                    Gson().toJson(movie)
                ) //converting entity data class to json to pass it through intent
                context.startActivity(intent)
            }

        }

        //convert the date in "yyyy-MM-dd" to Month Date, Year format
        @SuppressLint("SimpleDateFormat")
        fun stringToDate(dateString: String) :String{

            val originalFormat: DateFormat = SimpleDateFormat("yyyy-MM-dd")
            val targetFormat: DateFormat =
                SimpleDateFormat("MMMM dd, yyyy", Locale.US)

            val date :Date
            var formattedDate = ""
            try {
                date = originalFormat.parse(dateString)
                formattedDate = targetFormat.format(date)
            } catch (e: ParseException) {

            }
            return formattedDate
        }
    }


}