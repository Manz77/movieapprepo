package com.example.movieapp.view

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import androidx.lifecycle.Observer
import androidx.recyclerview.widget.LinearLayoutManager
import com.example.movieapp.R
import com.example.movieapp.model.Character
import com.example.movieapp.model.Movie
import com.example.movieapp.utils.NetworkState
import com.example.movieapp.viewmodel.SingleMovieViewModel
import com.google.gson.Gson
import kotlinx.android.synthetic.main.activity_single_movie.*

class SingleMovieActivity : AppCompatActivity() {

    private lateinit var singleMovieViewModel: SingleMovieViewModel
    private val characterAdapter = CharacterListAdapter(arrayListOf<Character>())

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_single_movie)

        //extract data passed from previous activity in an intent
        val jsonString = intent.getStringExtra("movie")
        val movie = Gson().fromJson(jsonString, Movie::class.java)

        singleMovieViewModel = SingleMovieViewModel(this.application, movie)

        characterList.apply {
            layoutManager = LinearLayoutManager(context)
            adapter = characterAdapter
        }


        supportActionBar?.title = movie.title
        director.text = movie.director
        producer.text = movie.producer

        observeSingleViewModel()

    }

    private fun observeSingleViewModel() {
        singleMovieViewModel.allCharacter.observe(this, Observer { characters ->
            characters?.let { characterAdapter.updateCharacters(characters) }
        })

        //observe networkState data to change the views accordingly
        singleMovieViewModel.networkState.observe(this, Observer {
            progress_bar.visibility = if(it == NetworkState.LOADING) View.VISIBLE else View.GONE
            txt_error.visibility = if(it == NetworkState.ERROR) View.VISIBLE else View.GONE
        })
    }
}