package com.example.movieapp.view

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.LinearLayoutManager
import com.example.movieapp.R
import com.example.movieapp.model.MoviesService
import com.example.movieapp.utils.NetworkState
import com.example.movieapp.viewmodel.MoviesListViewModel
import kotlinx.android.synthetic.main.activity_main.*

class MovieActivity : AppCompatActivity() {

    lateinit var viewModel: MoviesListViewModel
    private val moviesAdapter = MoviesListAdapter(arrayListOf())


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        viewModel = MoviesListViewModel(this.application)  //initiate movie ViewModel

        //setting recyclerview up
        moviesList.apply {
            layoutManager = LinearLayoutManager(context)
            adapter = moviesAdapter
        }

        observeMovieViewModel()
    }

    private fun observeMovieViewModel() {

        //observe movies data from viewModel
        viewModel.movies.observe(this, Observer { movies ->
            movies?.let { moviesAdapter.updateMovies(it) }
        })

        //observe networkState data to change the views accordingly
        viewModel.networkState.observe(this, Observer {
            loading_view.visibility = if(it == NetworkState.LOADING) View.VISIBLE else View.GONE
            list_error.visibility = if(it == NetworkState.ERROR) View.VISIBLE else View.GONE
            if(it == NetworkState.ERROR){
                moviesList.visibility = View.GONE
            }
        })
    }


}